import 'package:elemes_movie_app/app/config/colors.dart';
import 'package:elemes_movie_app/app/data/local/theme_service.dart';
import 'package:elemes_movie_app/app/ui/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static const primaryColor = Color(0xFF002171);
  static const whiteColor = Colors.white;
  static const blackColor = Colors.black;
  static const textColor = Color(0xff7E7E7E);
  static const greyColor = Color(0xFF868583);

  ThemeData appTheme = createTheme(
    brightness: Brightness.light,
    systemOverlayStyle: SystemUiOverlayStyle.dark,
    primarySwatch: primarySwatch,
    background: backgroundColor,
    primaryText: Colors.black,
    secondaryText: Colors.white,
    accentColor: secondaryColor,
    divider: secondaryColor,
    buttonBackground: Colors.black38,
    buttonText: secondaryColor,
    disabled: secondaryColor,
    error: Colors.red,
  );

  static ThemeData light(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return ThemeData(
      brightness: Brightness.light,
        primaryColor: primaryColor,
        appBarTheme: AppBarTheme(
            color: whiteColor,
            foregroundColor: Colors.black,
            iconTheme: IconThemeData(color: blackColor),
            elevation: 0.0),
        scaffoldBackgroundColor: Colors.white,
        fontFamily: GoogleFonts.roboto().fontFamily,
        textTheme: GoogleFonts.robotoTextTheme(textTheme).apply(bodyColor: textColor).copyWith(
              headline1: GoogleFonts.roboto(fontSize: 96, fontWeight: FontWeight.w300, letterSpacing: -1.5),
              headline2: GoogleFonts.roboto(fontSize: 60, fontWeight: FontWeight.w300, letterSpacing: -0.5),
              headline3: GoogleFonts.roboto(fontSize: 48, fontWeight: FontWeight.w400),
              headline4: GoogleFonts.roboto(fontSize: 34, fontWeight: FontWeight.bold,color: blackColor),
              headline5: GoogleFonts.roboto(fontSize: 24, fontWeight: FontWeight.bold),
              headline6: GoogleFonts.roboto(fontSize: 18, fontWeight: FontWeight.bold, ),
              subtitle1: GoogleFonts.lora(fontSize: 16, fontWeight: FontWeight.w500, ), ///dipake di form isian by
          ///default
              subtitle2: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w500,),
              // subtitle2: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.w700,), ///Dipakai untuk
              bodyText1: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w400,color: textColor),
              bodyText2: GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w400, color: textColor),
              button: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w500), /// dipakai juga untuk di nama user yang komentar
              caption: GoogleFonts.nunito(fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
              overline: GoogleFonts.roboto(fontSize: 10, fontWeight: FontWeight.w500,letterSpacing: 0.0), ///Untuk di Category label
            ),
        tabBarTheme: TabBarTheme(labelColor: primaryColor, labelStyle: GoogleFonts.roboto()),
        inputDecorationTheme: InputDecorationTheme(
          hintStyle: GoogleFonts.poppins(color: textColor),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: greyColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: greyColor),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: greyColor),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: greyColor),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            borderSide: BorderSide(color: Colors.red),
          ),
        ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        unselectedItemColor: Colors.grey,
        selectedItemColor: primaryColor,
        backgroundColor: whiteColor,
        selectedLabelStyle: GoogleFonts.poppins(color: primaryColor),
        unselectedLabelStyle: GoogleFonts.poppins(color: textColor),
        elevation: 0,
        showSelectedLabels: true,
        showUnselectedLabels: true,
      )
    );
  }
  static ThemeData dark(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return ThemeData(
        brightness: Brightness.dark,
        primaryColor: primaryColor,
        appBarTheme: AppBarTheme(
            color: blackColor,
            foregroundColor: whiteColor,
            iconTheme: IconThemeData(color: whiteColor),
            elevation: 0.0),
        scaffoldBackgroundColor: blackColor,
        textTheme: GoogleFonts.robotoTextTheme(textTheme).apply(bodyColor: textColor).copyWith(
          headline1: GoogleFonts.roboto(fontSize: 96, fontWeight: FontWeight.w300, letterSpacing: -1.5,color: whiteColor),
          headline2: GoogleFonts.roboto(fontSize: 60, fontWeight: FontWeight.w300, letterSpacing: -0.5,color: whiteColor),
          headline3: GoogleFonts.roboto(fontSize: 48, fontWeight: FontWeight.w400,color: whiteColor),
          headline4: GoogleFonts.roboto(fontSize: 34, fontWeight: FontWeight.bold,color: whiteColor),
          headline5: GoogleFonts.roboto(fontSize: 24, fontWeight: FontWeight.bold,color: whiteColor),
          headline6: GoogleFonts.roboto(fontSize: 18, fontWeight: FontWeight.bold, color: whiteColor),
          subtitle1: GoogleFonts.raleway(fontSize: 16, fontWeight: FontWeight.w500,color: whiteColor ), ///dipake di form
          ///isian by default
          // subtitle2: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w500,),
          subtitle2: GoogleFonts.roboto(fontSize: 12, fontWeight: FontWeight.w700,color: whiteColor), ///Dipakai untuk
          bodyText1: GoogleFonts.roboto(fontSize: 16, fontWeight: FontWeight.w400,color: whiteColor),
          bodyText2: GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w400, color: whiteColor),
          button: GoogleFonts.roboto(fontSize: 14, fontWeight: FontWeight.w500,color: whiteColor), /// dipakai juga untuk di nama user yang komentar
          caption: GoogleFonts.nunito(fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4,color: whiteColor),
          overline: GoogleFonts.roboto(fontSize: 10, fontWeight: FontWeight.w500,letterSpacing: 0.0,color: whiteColor), ///Untuk di Category label
        ),
        tabBarTheme: TabBarTheme(labelColor: primaryColor, labelStyle: GoogleFonts.roboto()),
      inputDecorationTheme: InputDecorationTheme(

        hintStyle: GoogleFonts.poppins(color: whiteColor),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          borderSide: BorderSide(color: whiteColor),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          borderSide: BorderSide(color: whiteColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          borderSide: BorderSide(color: whiteColor),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          borderSide: BorderSide(color: whiteColor),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          borderSide: BorderSide(color: Colors.red),
        ),
      ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          unselectedItemColor: Colors.grey,
          selectedItemColor: primaryColor,
          backgroundColor: blackColor,
          selectedLabelStyle: GoogleFonts.poppins(color: primaryColor),
          unselectedLabelStyle: GoogleFonts.poppins(color: textColor),
          elevation: 0,
          showSelectedLabels: true,
          showUnselectedLabels: true,
        )
    );
  }



  ///Detail Berita
  static TextStyle newsTitleHeaderBig = GoogleFonts.roboto(
    fontWeight: FontWeight.w600,
    // color: Colors.black,
    color: ThemeService().theme == ThemeMode.dark ? whiteColor : Colors.red,
    fontSize: 22,
  );
  static TextStyle newsTitleHeaders = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: ThemeService().theme == ThemeMode.dark ? whiteColor : Colors.black,
  );

  ///untuk timer e.g 10menit yang lalu
  // static TextStyle timerTextStyle = GoogleFonts.roboto(
  //   fontSize: 12,
  //   color: ThemeService().theme == ThemeMode.dark ? whiteColor : Colors.black,
  // );

  static TextStyle newsTitleHeader = GoogleFonts.roboto(
    fontSize: 14,
    color: ThemeService().theme == ThemeMode.dark ? whiteColor : Colors.black,
  );
}

Text headLine1Text({required String text}) {
  return Text(text, style: headLineTextStyle);
}

final headLineTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: Colors.red,
);
