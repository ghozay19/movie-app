import 'package:colour/colour.dart';
import 'package:flutter/material.dart';

// MaterialColor primarySwatch = Colors.blue;
MaterialColor primarySwatch = Colors.blue;
Color backgroundColor = Colour('#F9F9F9');
Color secondaryColor = Colour('#5E92F3');

//Nusantara Tv
Color primaryColor = Colour('#002171');
const whiteColor = Colors.white;
const blackColor = Colors.black;
const textColor = Color(0xff7E7E7E);
const greyColor = Color(0xFF868583);
