const String appLogo = 'assets/images/icon_movie.png';
const String placeHolder = 'assets/images/placeholder.png';

String connectionErrorAnimation = './assets/images/no_connection.json';
String emptyAnimation = './assets/images/empty-state.json';
String failedLoadData = './assets/images/failedloaddata.png';