import 'package:elemes_movie_app/app/modules/dashboard/binding/dashboard_binding.dart';
import 'package:elemes_movie_app/app/modules/detail/detail_movie_binding.dart';
import 'package:elemes_movie_app/app/modules/search_movie/search_movie_binding.dart';
import 'package:elemes_movie_app/app/ui/screens/dashboard_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/detail_movie_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/list_movie/list_now_playing_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/list_movie/list_top_rated_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/list_movie/list_upcoming_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/list_movie/list_trending_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/search_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/splash_screen.dart';
import 'package:get/get.dart';

class Routes {
  static const String root = '/';
  static const String dashboard = '/dashboard';
  static const String detail = '/detail';
  static const String search = '/search';
  static const String listNowPlaying = '/list-now-playing';
  static const String listUpComing = '/list-up-coming';
  static const String listTopRated = '/list-top-rated';
  static const String listTrending = '/list-trending';

}
final List<GetPage> routes = [
  GetPage(
    name: Routes.root,
    page: () => SplashScreen(),
  ),
  GetPage(
    name: Routes.dashboard,
    page: () => DashboardScreen(),
    binding: DashboardBinding()
  ),
  GetPage(
    name: Routes.detail,
    page: () => DetailMovieScreen(),
    binding: DetailMovieBinding()
  ),
  GetPage(
    name: Routes.search,
    page: () => SearchScreen(),
    binding: SearchMovieBinding()
  ),
  GetPage(
    name: Routes.listNowPlaying,
    page: () => ListNowPlayingScreen(),
  ),
  GetPage(
    name: Routes.listUpComing,
    page: () => ListUpcomingScreen(),
  ),
  GetPage(
    name: Routes.listTopRated,
    page: () => ListTopRatedScreen(),
  ),
  GetPage(
    name: Routes.listTrending,
    page: () => ListTrendingScreen(),
  ),
];
