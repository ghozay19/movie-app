import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class ElemesMovieApp extends StatelessWidget {
  const ElemesMovieApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.root,
      getPages: routes,
      theme: AppTheme.dark(context),
      // initialBinding: BaseBinding(),
      locale: Get.deviceLocale,
      fallbackLocale: const Locale('en', 'US'),
    );
  }
}
