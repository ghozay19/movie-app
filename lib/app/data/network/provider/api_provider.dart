

import 'package:elemes_movie_app/app/data/network/api_constants.dart';
import 'package:elemes_movie_app/app/data/network/model/detail/detail_movie_response.dart';
import 'package:elemes_movie_app/app/data/network/model/movie_credits/movie_credits_response.dart';
import 'package:elemes_movie_app/app/data/network/model/movie_response.dart';
import 'package:elemes_movie_app/app/data/network/service/service.dart';

class ApiProvider {

  final _apiService = ApiService(baseUrl);


  Future<MovieResponse> doGetNowPlayingMovie(
      {int? page}) async {
    final response = await _apiService.call('/movie/now_playing/'+'$apiKey' +'$language'+'page=$page'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<MovieResponse> doGetUpComingMovie(
      {int? page}) async {
    final response = await _apiService.call('/movie/upcoming/'+'$apiKey' +'$language'+'page=$page'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<MovieResponse> doGetPopularMovie(
      {int? page}) async {
    final response = await _apiService.call('/movie/popular/'+'$apiKey' +'$language'+'page=$page'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<MovieResponse> doGetTopRatedMovie(
      {int? page}) async {
    final response = await _apiService.call('/movie/top_rated/'+'$apiKey' +'$language'+'page=$page'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<MovieResponse> doGetTrendingMovie(
      {String? type,int? page}) async {
    final response = await _apiService.call('/trending/movie/$type'+'$apiKey' +'$language'+'page=$page'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<MovieResponse> doSearchMovie(
      {String? query,int? page}) async {
    final response = await _apiService.call('/search/movie'+'$apiKey'
        +'$language'+'&query=$query'+'&page=$page',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieResponse.fromJson(response?.data);
    } else {
      return MovieResponse();
    }
  }

  Future<DetailMovieResponse?> doDetailMovie(
      {int? id}) async {
    final response = await _apiService.call('/movie/$id'+'$apiKey'+'$language'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return DetailMovieResponse.fromJson(response?.data);
    } else {
      return DetailMovieResponse();
    }
  }

  Future<MovieCreditsResponse?> doGetMovieCredits(
      {int? id}) async {
    final response = await _apiService.call('/movie/$id'+'/credits'+'$apiKey'+'$language'+'region=ID',
        request: {},
        method: MethodRequest.GET);
    if (response?.statusCode == 200) {
      return MovieCreditsResponse.fromJson(response?.data);
    } else {
      return MovieCreditsResponse();
    }
  }

}