import 'results.dart';

class MovieResponse {
  MovieResponse({
      this.page, 
      this.movies,
      this.totalPages, 
      this.totalResults,});

  MovieResponse.fromJson(dynamic json) {
    page = json['page'];
    if (json['results'] != null) {
      movies = [];
      json['results'].forEach((v) {
        movies?.add(Movie.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }
  int? page;
  List<Movie>? movies;
  int? totalPages;
  int? totalResults;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['page'] = page;
    if (movies != null) {
      map['results'] = movies?.map((v) => v.toJson()).toList();
    }
    map['total_pages'] = totalPages;
    map['total_results'] = totalResults;
    return map;
  }

  @override
  String toString() {
    return 'MovieResponse{page: $page, movies: $movies, totalPages: $totalPages, totalResults: $totalResults}';
  }
}