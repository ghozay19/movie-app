const baseUrl = 'https://api.themoviedb.org/3';
const apiKey = '?api_key=ed1f1b69630ec5a7109bca52edb898f9';
const language = '&language=en-US';
const region = '&region=ID';
const imageUrl = 'http://image.tmdb.org/t/p/original';
const imageUrlW500 = 'http://image.tmdb.org/t/p/w500';
const errorImage = "https://image.shutterstock.com/image-vector/picture-vector-icon-no-image-260nw-1350441335.jpg";
