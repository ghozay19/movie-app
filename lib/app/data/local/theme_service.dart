import 'package:elemes_movie_app/app/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:logger/logger.dart';


class ThemeService {
  final _box = GetStorage();
  final key = 'isDarkMode';
  final notifKey = 'notifKey';

  ThemeMode get theme => _loadThemeFromBox() ? ThemeMode.dark : ThemeMode.light;
  bool get isDarkMode => _loadThemeFromBox() ? true : false;
  bool get isNotifOn => _loadNotifFromBox() ? true : false;

  bool _loadThemeFromBox() => _box.read(key) ?? false;
  bool _loadNotifFromBox() => _box.read(notifKey) ?? false;

  _saveThemeToBox(bool isDarkMode) {
    if (isDarkMode == true) {}
    _box.write(key, isDarkMode);
  }

  _saveNotifToBox(bool isNotifOn) {
    _box.write(key, isNotifOn);
  }
  ///Will Go through the dashboard
  void switchTheme() {
    var logger = Logger();
    logger.d('theme ${_loadThemeFromBox()} ');
    Get.offAllNamed(Routes.dashboard);
    Get.changeThemeMode(
      _loadThemeFromBox() ? ThemeMode.light : ThemeMode.dark,
    );
    _saveThemeToBox(!_loadThemeFromBox());
  }

  void switchThemeOnly() {
    var logger = Logger();
    logger.d('theme ${_loadThemeFromBox()} ');
    Get.changeThemeMode(
      _loadThemeFromBox() ? ThemeMode.light : ThemeMode.dark,
    );
    _saveThemeToBox(!_loadThemeFromBox());
  }

}
