import 'package:flutter/material.dart';
import 'package:intl/intl.dart';



String formatDate(String? date) {
  final formatDate = new DateFormat('d MMM yyyy','en');
  var dateNull;
  if(date==null||date==''){
    dateNull = DateTime.now().toString();
  }
  return formatDate.format(DateTime.parse(date??dateNull));
}

String formatDateYearOnly(String? date) {
  final formatDate = new DateFormat('MMMM yyyy','en');
  var dateNull;
  if(date==null||date==''){
    dateNull = DateTime.now().toString();
  }
  return formatDate.format(DateTime.parse(date??dateNull));
}