import 'package:elemes_movie_app/app/config/assets.dart';
import 'package:elemes_movie_app/app/ui/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class EmptyState extends StatelessWidget {
  final String? message;
  const EmptyState({
    Key? key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: screenWidth(context),
        height: screenHeight(context,dividedBy: 6),
        padding: EdgeInsets.symmetric(
            horizontal: 16), //TODO
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // Spacer(),
            Lottie.asset(
              emptyAnimation,
              width: screenWidth(context),
              height: screenHeight(context,dividedBy: 9),
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: 16,
            ),
            Text(message ?? "No Data Founds",
                style: Theme.of(context).textTheme.button),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
