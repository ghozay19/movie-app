import 'package:cached_network_image/cached_network_image.dart';
import 'package:elemes_movie_app/app/config/assets.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MovieImageNetwork extends StatelessWidget {
  final String? imageUrl;
  final double? height;
  final double? width;
  final BoxFit? fit;
  final BorderRadius? borderRadius;
  const MovieImageNetwork(
      {Key? key,
      this.imageUrl,
      this.height,
      this.width,
      this.fit,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ClipRRect(
      borderRadius: borderRadius ?? BorderRadius.all(Radius.circular(8)),
      child: CachedNetworkImage(
        width: width,
        height: height,
        imageUrl: imageUrl??'',
        fit: fit ?? BoxFit.contain,
        errorWidget: (context, url, error) {
          return Image.asset(
            placeHolder,
            fit: fit ?? BoxFit.cover,
            width: height,
            height: width,
          );
        },
        placeholder: (context, url) => Shimmer.fromColors(
          baseColor: Colors.black12,
          highlightColor: Colors.white10,
          child: Container(
              decoration: const BoxDecoration(
                color: Colors.amberAccent,
              )),
        ),
      ),
    );
  }
}
