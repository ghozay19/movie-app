import 'package:elemes_movie_app/app/config/assets.dart';
import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/ui/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:lottie/lottie.dart';

class RowErrorState extends StatelessWidget {
  final VoidCallback? onRefresh;
  const RowErrorState({
    Key? key,
    this.onRefresh,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        // height: 70,
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 5)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Lottie.asset(
              connectionErrorAnimation,
              height: 75,
              fit: BoxFit.contain,
            ),
            Gap(8),
            Text("Gagal memuat",
                style: Theme.of(context).textTheme.caption),
            Gap(4),
            InkResponse(
              onTap: onRefresh,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.refresh,
                    color: AppTheme.primaryColor,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Refresh",
                      style: TextStyle(
                          color: AppTheme.primaryColor,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 12.0))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
