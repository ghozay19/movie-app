import 'package:elemes_movie_app/app/config/assets.dart';
import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/data/local/theme_service.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';


class ErrorMessage extends GetWidget {
  final String message;
  final void Function() onRefresh;

  const ErrorMessage(
    this.message, {
    Key? key,
    required this.onRefresh,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(width: 72, height: 50, child: Image.asset(failedLoadData)),
          const Gap(13),
          Text(message, style: Theme.of(context).textTheme.button),
          const Gap(2),
          TextButton(
            style: TextButton.styleFrom(
                // padding: EdgeInsets.zero,
                minimumSize: Size(50, 30),
                alignment: Alignment.centerLeft),
            onPressed: onRefresh,
            child: Text('Refresh',
              style: TextStyle(color: ThemeService().theme == ThemeMode.dark ? AppTheme.whiteColor : AppTheme.primaryColor,),
            ),
          )
        ],
      ),
    );
  }
}
