import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/ui/utils/size.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerHeaderMovie extends StatelessWidget {
  const ShimmerHeaderMovie({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Shimmer.fromColors(
            child: Container(
              height: screenHeight(context, dividedBy: 4),
              width: screenWidth(context),
              decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
              child: Center(
                child: Text('Not Found'),
              ),
            ),
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
          ),
        ],
      ),
    );
  }
}

class ShimmerHorizontalItem extends StatelessWidget {
  const ShimmerHorizontalItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Container(
            width: 120,
            height: 100,
            decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
            child: Center(
              child: Text(
                'Not Found',
                style: AppTheme.newsTitleHeaderBig,
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            child: Container(
              height: 10,
              width: 110,
              decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
              child: Center(
                child: Text(
                  'Not Found',
                  style: AppTheme.newsTitleHeaderBig,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ShimmerVerticalItem extends StatelessWidget {
  const ShimmerVerticalItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: Container(
        height: 110,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                width: 120,
                height: 110,
                decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
                child: Center(
                  child: Text(
                    'Not Found',
                    style: AppTheme.newsTitleHeaderBig,
                  ),
                ),
              ),
            ),
            Flexible(
              child: SizedBox(
                height: 100,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 0,horizontal: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        child: Container(
                          decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
                          child: Center(
                            child: Text('Not Found'),
                          ),
                        ),
                      ),
                      Gap(8),
                      Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        child: Container(
                          decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
                          child: Center(
                            child: Text('Not Found'),
                          ),
                        ),
                      ),
                      Spacer(),
                      Shimmer.fromColors(
                        baseColor: Colors.grey[300]!,
                        highlightColor: Colors.grey[100]!,
                        child: Container(
                          decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
                          child: Center(
                            child: Text('Not Found'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(color: AppTheme.whiteColor, borderRadius: BorderRadius.all(Radius.circular(4))),
                  child: Center(
                    child: Text('Not Found'),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}