import 'package:auto_size_text/auto_size_text.dart';
import 'package:colour/colour.dart';
import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/data/network/api_constants.dart';
import 'package:elemes_movie_app/app/data/local/models/genre.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gap/gap.dart';
import 'package:google_fonts/google_fonts.dart';


class MovieItemWidget extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final  String? posterPath;
  final num? voteAverage;
  final List<int>? genreIds;

  MovieItemWidget({
    Key? key,
    this.onTap,
    this.title,
    this.posterPath,
    this.genreIds,
    this.voteAverage,
  }) : super(key: key);

  // final Movie data;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                Expanded(
                    child: Container(
                      height: 120,
                      decoration: BoxDecoration(
                        color: AppTheme.greyColor,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.only(top: 10.0, left: 80, right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AutoSizeText(
                                '$title',
                                style: Theme.of(context).textTheme.subtitle1?.copyWith(
                                  color: AppTheme.whiteColor,
                                  fontSize: 18,
                                ),
                                maxLines: 1,
                                softWrap: true,
                              ),
                              Gap(4),
                              if (voteAverage != null)
                                Row(
                                  children: [
                                    RatingBarIndicator(
                                      rating: voteAverage! / 2.0,
                                      itemCount: 5,
                                      itemBuilder: (context, index) => Icon(
                                        Icons.star,
                                        color: Colors.yellow,
                                      ),
                                      itemSize: 24,
                                    ),
                                    Gap(8),
                                    Text('$voteAverage'),
                                    Gap(8),
                                  ],
                                ),
                              Gap(4),
                              if (genreIds != null)
                                Wrap(
                                  children: List<Widget>.generate(
                                    genreIds!.length,
                                        (index) => Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 2, vertical: 2),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: AppTheme.primaryColor,
                                            borderRadius: BorderRadius.circular(
                                                20) // use instead of BorderRadius.all(Radius.circular(20))
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                                          child: Text(
                                            '${Genres.genres[genreIds![index]]}',
                                            style: GoogleFonts.roboto(color: AppTheme.whiteColor, fontSize: 12),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              // Text(formatDate(data.releaseDate)),
                            ],
                          )),
                    ))
              ],
            ),
          ),
          Positioned(
            top: 20,
            left: 5,
            child: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colour('#DDDDDD')),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                      posterPath != null ? imageUrlW500 + '$posterPath' : errorImage,
                    ),
                    radius: 40,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}