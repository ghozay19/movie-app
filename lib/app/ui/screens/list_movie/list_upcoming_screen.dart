import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/data/local/theme_service.dart';
import 'package:elemes_movie_app/app/modules/now_playing_movie/nowplaying_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/upcoming_movie/upcoming_movie_controller.dart';
import 'package:elemes_movie_app/app/routes/routes.dart';
import 'package:elemes_movie_app/app/ui/screens/detail_movie_screen.dart';
import 'package:elemes_movie_app/app/ui/widget/global_error_message.dart';
import 'package:elemes_movie_app/app/ui/widget/loading_more.dart';
import 'package:elemes_movie_app/app/ui/widget/movie_item_widget.dart';
import 'package:elemes_movie_app/app/ui/widget/row_error_state.dart';
import 'package:elemes_movie_app/app/ui/widget/shimmer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ListUpcomingScreen extends GetView<UpComingMovieController> {
  const ListUpcomingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Upcoming',
          style: GoogleFonts.roboto(color: AppTheme.whiteColor),
        ),
      ),
      body: Obx(() {
        if (controller.isFailed) {
          return RowErrorState(onRefresh:()=> controller.getUpcomingMovie(isRefresh: true));
        }
        if (controller.isEmpty) {
          return ErrorMessage('Failed to load data', onRefresh:()=> controller.getUpcomingMovie(isRefresh: true));
        }
        if (controller.isLoadingFirst) {
          return ShimmerVerticalItem();
        }
        return ScrollablePositionedList.builder(
            itemScrollController: controller.itemScrollController,
            itemPositionsListener: controller.itemPositionsListener,
            itemCount: controller.count + 1,
            itemBuilder: (context, index) {
              bool isItem = index < controller.count;
              bool isLastIndex = index == controller.count;
              bool isLoadingMore = isLastIndex && controller.isLoadingMore;

              // User Item
              if (isItem) {
                var data = controller.items[index];
                  return MovieItemWidget(
                    title: data.title,
                    voteAverage: data.voteAverage,
                    genreIds: data.genreIds,
                    posterPath: data.posterPath,
                    onTap: () {
                      Get.toNamed(
                        Routes.detail,
                        arguments: DetailMovieArgument(id: data.id),
                      );
                    },
                  );
              }

              // Show loading more at the bottom
              if (isLoadingMore) return const LoadingMore();

              // Default empty content
              return Container();
            });
      }),
    );
  }
}
