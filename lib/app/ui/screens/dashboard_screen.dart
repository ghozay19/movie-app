import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/data/local/models/genre.dart';
import 'package:elemes_movie_app/app/data/network/api_constants.dart';
import 'package:elemes_movie_app/app/modules/dashboard/controller/dashboard_controller.dart';
import 'package:elemes_movie_app/app/modules/now_playing_movie/nowplaying_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/popular_movie/popular_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/top_rated_movie/top_rated_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/trending_movie/trending_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/upcoming_movie/upcoming_movie_controller.dart';
import 'package:elemes_movie_app/app/routes/routes.dart';
import 'package:elemes_movie_app/app/ui/screens/detail_movie_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/list_movie/list_now_playing_screen.dart';
import 'package:elemes_movie_app/app/ui/utils/date_formatter.dart';
import 'package:elemes_movie_app/app/ui/utils/size.dart';
import 'package:elemes_movie_app/app/ui/widget/empty_state.dart';
import 'package:elemes_movie_app/app/ui/widget/image_network.dart';
import 'package:elemes_movie_app/app/ui/widget/row_error_state.dart';
import 'package:elemes_movie_app/app/ui/widget/shimmer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';

class DashboardScreen extends GetView<DashboardController> {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var popularMovie = Get.find<PopularMovieController>();
    var upComing = Get.find<UpComingMovieController>();
    var nowPlaying = Get.find<NowPlayingMovieController>();
    var topRated = Get.find<TopRatedMovieController>();
    var trending = Get.find<TrendingMovieController>();
    var logger = Logger();
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            ///Popular
            Obx(
              () {
                if (popularMovie.listSliderNews.isNotEmpty) {
                  return Column(
                    children: [
                      Stack(
                        children: [
                          CarouselSlider(
                            carouselController: popularMovie.carouselController,
                            options: CarouselOptions(
                              onPageChanged: (index, reason) {
                                popularMovie.currentSlider.value = index;
                              },
                              enlargeCenterPage: false,
                              autoPlay: true,
                              viewportFraction: 1,
                              height: screenHeight(context, dividedBy: 1.7),
                            ),
                            items: popularMovie.listSliderNews
                                .map((data) => InkWell(
                              onTap: () {
                                Get.toNamed(
                                  Routes.detail,
                                  arguments: DetailMovieArgument(id: data.id),
                                );
                              },
                              child: Container(
                                  width: Get.width,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Stack(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(12.0),
                                            ),
                                            child: MovieImageNetwork(
                                              imageUrl: '$imageUrl${data.backdropPath}',
                                              fit: BoxFit.cover,
                                              height: screenHeight(context, dividedBy: 1.7),
                                              borderRadius: BorderRadius.all(Radius.circular(6)),
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            child: Container(
                                              height: 350.0,
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                gradient: LinearGradient(
                                                  begin: FractionalOffset.topCenter,
                                                  end: FractionalOffset.bottomCenter,
                                                  colors: [
                                                    Colors.grey.withOpacity(0.0),
                                                    Colors.black,
                                                  ],
                                                  stops: [0.0, 1.0],
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            child: Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.all(2.0),
                                                  child: AutoSizeText(
                                                    '${data.title ?? ''}',
                                                    maxLines: 2,
                                                    softWrap: true,
                                                    textAlign: TextAlign.center,
                                                    maxFontSize: 30,
                                                    style: Theme.of(context).textTheme.headline4?.copyWith(
                                                      color: AppTheme.whiteColor,
                                                      fontSize: 60,
                                                    ),
                                                  ),
                                                ),
                                                Text(formatDateYearOnly(data.releaseDate)),
                                                if(data.genreIds!=null)
                                                  Wrap(
                                                  children: List<Widget>.generate(
                                                    data.genreIds!.length,
                                                        (index) => Padding(
                                                          padding: EdgeInsets.symmetric(horizontal: 4,vertical: 2),
                                                      child: Text(
                                                        '• ${Genres.genres[data.genreIds![index]]}',
                                                        style: GoogleFonts.roboto(color: AppTheme.whiteColor, fontSize: 12),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding: const EdgeInsets.all(16.0),
                                                  child: AutoSizeText(
                                                    '${data.overview ?? ''}',
                                                    maxLines: 3,
                                                    softWrap: true,
                                                    overflow: TextOverflow.ellipsis,
                                                    textAlign: TextAlign.center,
                                                    maxFontSize: 18,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .caption
                                                        ?.copyWith(color: AppTheme.whiteColor),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  )),
                            ))
                                .toList(),
                          ),
                          Positioned(
                            top: MediaQuery.of(context).padding.top,
                            right: 16,
                            child: SizedBox(
                            child: InkWell(
                              onTap: () => Get.toNamed(Routes.search),
                              child: Icon(
                                Icons.search,
                                color: AppTheme.whiteColor,
                                size: 40,
                              ),
                            ),
                          ),),
                        ],
                      )
                    ],
                  );
                }
                if (popularMovie.isSliderLoading) {
                  return ShimmerHeaderMovie();
                }
                if (popularMovie.isSliderFailed) {
                  return RowErrorState(
                    onRefresh: () => popularMovie.getPopularSliderMovie(isRefresh: true),
                  );
                }
                if (popularMovie.isSliderEmpty) {
                  return EmptyState(
                    message: 'Movie Not Found',
                  );
                }
                return Container();
              },
            ),

            ///Now Playing
            Obx(
              () {
                if (nowPlaying.listSlider.isNotEmpty) {
                  return Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Now Playing Movie',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: AppTheme.greyColor,
                              size: 15,
                            ),
                            onPressed: () {
                              Get.toNamed(Routes.listNowPlaying);
                            },
                          )
                        ],
                      ),
                      Container(
                        height: 200,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: nowPlaying.listSlider.length,
                          itemBuilder: (context, index) {
                            var data = nowPlaying.listSlider[index];
                            return InkWell(
                              onTap: () {
                                Get.toNamed(
                                  Routes.detail,
                                  arguments: DetailMovieArgument(id: data.id),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8),
                                child: MovieImageNetwork(
                                  imageUrl: '$imageUrl${data.posterPath}',
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                }
                if (nowPlaying.isSliderLoading) {
                  return ShimmerHorizontalItem();
                }
                if (nowPlaying.isSliderFailed) {
                  return RowErrorState(
                    onRefresh: () => nowPlaying.getNowPlayingSliderMovie(isRefresh: true),
                  );
                }
                if (nowPlaying.isSliderEmpty) {
                  return EmptyState(
                    message: 'Movie Not Found',
                  );
                }
                return Container();
              },
            ),

            ///Up coming
            Obx(
              () {
                if (upComing.listSliderNews.isNotEmpty) {
                  return Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Upcoming Movie',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: AppTheme.greyColor,
                              size: 15,
                            ),
                            onPressed: () {
                              Get.toNamed(Routes.listUpComing);
                            },
                          )
                        ],
                      ),
                      Container(
                        height: 200,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: upComing.listSliderNews.length,
                          itemBuilder: (context, index) {
                            var data = upComing.listSliderNews[index];
                            return InkWell(
                              onTap: () {
                                Get.toNamed(
                                  Routes.detail,
                                  arguments: DetailMovieArgument(id: data.id),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8),
                                child: MovieImageNetwork(
                                  imageUrl: '$imageUrl${data.posterPath}',
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                }
                if (upComing.isSliderLoading) {
                  return ShimmerHorizontalItem();
                }
                if (upComing.isSliderFailed) {
                  return RowErrorState(
                    onRefresh: () => upComing.getUpcomingSliderMovie(isRefresh: true),
                  );
                }
                if (upComing.isSliderEmpty) {
                  return EmptyState(
                    message: 'Movie Not Found',
                  );
                }
                return Container();
              },
            ),

            ///TopRated
            Obx(
              () {
                if (topRated.listSliderNews.isNotEmpty) {
                  return Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Top Rated Movie',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: AppTheme.greyColor,
                              size: 15,
                            ),
                            onPressed: () {
                              Get.toNamed(Routes.listTopRated);
                            },
                          )
                        ],
                      ),
                      CarouselSlider(
                        carouselController: topRated.carouselController,
                        options: CarouselOptions(
                          onPageChanged: (index, reason) {
                            topRated.currentSlider.value = index;
                          },
                          enlargeCenterPage: false,
                          autoPlay: true,
                          viewportFraction: 1,
                          height: 260,
                        ),
                        items: topRated.listSliderNews
                            .map((data) => InkWell(
                                  onTap: () {
                                    Get.toNamed(
                                      Routes.detail,
                                      arguments: DetailMovieArgument(id: data.id),
                                    );
                                  },
                                  child: Container(
                                      padding: EdgeInsets.all(4),
                                      width: Get.width,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Stack(
                                            children: [
                                              Card(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(12.0),
                                                  ),
                                                  child: MovieImageNetwork(
                                                    imageUrl: '$imageUrl${data.backdropPath}',
                                                    fit: BoxFit.cover,
                                                    height: 240,
                                                    borderRadius: BorderRadius.all(Radius.circular(6)),
                                                  )),
                                              Positioned(
                                                bottom: 0,
                                                left: 0,
                                                right: 0,
                                                child: Padding(
                                                  padding: EdgeInsets.all(4),
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        color: Colors.black.withOpacity(0.3),
                                                        borderRadius: const BorderRadius.only(
                                                          bottomLeft: Radius.circular(4),
                                                          bottomRight: Radius.circular(4),
                                                        )),
                                                    child: Padding(
                                                      padding: EdgeInsets.all(8),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          AutoSizeText(
                                                            '${data.title ?? ''}',
                                                            maxLines: 3,
                                                            softWrap: true,
                                                            // minFontSize: 15,
                                                            maxFontSize: 18,
                                                            style: Theme.of(context)
                                                                .textTheme
                                                                .headline6
                                                                ?.copyWith(color: AppTheme.whiteColor),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      )),
                                ))
                            .toList(),
                      ),
                    ],
                  );
                }
                if (topRated.isSliderLoading) {
                  return ShimmerHeaderMovie();
                }
                if (topRated.isSliderFailed) {
                  return RowErrorState(
                    onRefresh: () => topRated.getTopRatedSliderMovie(isRefresh: true),
                  );
                }
                if (topRated.isSliderEmpty) {
                  return EmptyState(
                    message: 'Movie Not Found',
                  );
                }
                return Container();
              },
            ),

            ///trending
            Obx(
              () {
                if (trending.listSliderNews.isNotEmpty) {
                  return Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Trending on This Week',
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: AppTheme.greyColor,
                              size: 15,
                            ),
                            onPressed: () {
                              Get.toNamed(Routes.listTrending);
                            },
                          )
                        ],
                      ),
                      Container(
                        height: 300,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: trending.listSliderNews.length,
                          itemBuilder: (context, index) {
                            var data = trending.listSliderNews[index];
                            return InkWell(
                              onTap: () {
                                Get.toNamed(
                                  Routes.detail,
                                  arguments: DetailMovieArgument(id: data.id),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8),
                                child: MovieImageNetwork(
                                  imageUrl: '$imageUrl${data.posterPath}',
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                }
                if (trending.isSliderLoading) {
                  return ShimmerHorizontalItem();
                }
                if (trending.isSliderFailed) {
                  return RowErrorState(
                    onRefresh: () => trending.getTrendingWeekSliderMovie(isRefresh: true),
                  );
                }
                if (trending.isSliderEmpty) {
                  return EmptyState(
                    message: 'Movie Not Found',
                  );
                }
                return Container();
              },
            ),
          ],
        ),
      ),
    );
  }
}
