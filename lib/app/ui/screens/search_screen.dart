import 'package:elemes_movie_app/app/modules/search_movie/search_movie_controller.dart';
import 'package:elemes_movie_app/app/routes/routes.dart';
import 'package:elemes_movie_app/app/ui/screens/detail_movie_screen.dart';
import 'package:elemes_movie_app/app/ui/screens/search_bar.dart';
import 'package:elemes_movie_app/app/ui/widget/empty_state.dart';
import 'package:elemes_movie_app/app/ui/widget/loading_more.dart';
import 'package:elemes_movie_app/app/ui/widget/movie_item_widget.dart';
import 'package:elemes_movie_app/app/ui/widget/row_error_state.dart';
import 'package:elemes_movie_app/app/ui/widget/shimmer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class SearchScreen extends GetView<SearchMovieController> {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SearchBar(
        onSubmitted: (val) {
          controller.queryText.value = val;
          controller.doSearchNews(isRefresh: false, isSearch: true, query: val);
        },
        onChanged: (val) {},
      ),
      body: Obx(() {
        if (controller.isFirstInit) {
          return EmptyState(message: "Oops! You haven't entered your search keywords yet");
        }
        if (controller.isLoadingFirst) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 3,
                itemBuilder: (context, index) => ShimmerVerticalItem()),
          );
        }
        if (controller.isFailed) {
          return RowErrorState(onRefresh: () => controller.doSearchNews(isRefresh: true, isSearch: false));
        }
        if (controller.isEmpty) {
          return EmptyState(
            message: 'Oops! Movie not found',
          );
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Obx(() {
              if (controller.isLoading) return Container();
              if (controller.searchNewsResponse?.totalResults == null) return Container();
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '${controller.searchNewsResponse?.totalResults} Movie Found',
                  style: Theme.of(context).textTheme.button,
                ),
              );
            }),
            Expanded(
              child: ScrollablePositionedList.builder(
                  itemScrollController: controller.itemScrollController,
                  itemPositionsListener: controller.itemPositionsListener,
                  itemCount: controller.count + 1,
                  itemBuilder: (context, index) {
                    bool isItem = index < controller.count;
                    bool isLastIndex = index == controller.count;
                    bool isLoadingMore = isLastIndex && controller.isLoadingMore;

                    // User Item
                    if (isItem) {
                      var data = controller.items[index];
                      return MovieItemWidget(
                        title: data.title,
                        voteAverage: data.voteAverage,
                        genreIds: data.genreIds,
                        posterPath: data.posterPath,
                        onTap: () {
                          Get.toNamed(
                            Routes.detail,
                            arguments: DetailMovieArgument(id: data.id),
                          );
                        },
                      );
                    }

                    // Show loading more at the bottom
                    if (isLoadingMore) return const LoadingMore();

                    // Default empty content
                    return Container();
                  }),
            ),
          ],
        );
      }),
    );
  }
}

