import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchBar extends StatelessWidget implements PreferredSizeWidget {
  final String? hintText;
  final Function(String) onSubmitted;
  final Function(String) onChanged;
  final Widget? title;

  const SearchBar({
    Key? key,
    required this.onSubmitted,
    required this.onChanged,
    this.title,
    this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: [
                BackButton(
                  color: AppTheme.whiteColor,
                ),
                Flexible(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: AppTheme.textColor,
                        ),
                        color: AppTheme.whiteColor,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Row(
                      children: <Widget>[
                        // https://api.themoviedb.org/3/search/movie?api_key=ed1f1b69630ec5a7109bca52edb898f9&language=en-US&query=naruto&page=1&include_adult=false
                        Expanded(
                          child: TextField(
                            textInputAction: TextInputAction.go,
                            style: GoogleFonts.roboto(
                              color: AppTheme.blackColor,
                            ),
                            onSubmitted: (val) => onSubmitted(val),
                            onChanged: (val) => onChanged(val),
                            decoration: InputDecoration(
                              hintStyle: GoogleFonts.roboto(
                                color:AppTheme.blackColor,
                              ),
                              hintText: hintText ?? 'Search Movie',
                              border: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              focusedErrorBorder: InputBorder.none,
                            ),
                          ),
                        ),
                        Icon(
                          Icons.search,
                          color: Colors.black54,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            if (title != null)
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 16,
                  ),
                  Flexible(child: title!),
                ],
              )
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(150);
}
