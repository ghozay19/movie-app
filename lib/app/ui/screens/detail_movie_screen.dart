import 'package:auto_size_text/auto_size_text.dart';
import 'package:elemes_movie_app/app/config/themes.dart';
import 'package:elemes_movie_app/app/data/network/api_constants.dart';
import 'package:elemes_movie_app/app/modules/detail/detail_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/movie_credits/movie_credits_controller.dart';
import 'package:elemes_movie_app/app/ui/utils/date_formatter.dart';
import 'package:elemes_movie_app/app/ui/utils/size.dart';
import 'package:elemes_movie_app/app/ui/widget/image_network.dart';
import 'package:elemes_movie_app/app/ui/widget/shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:gap/gap.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailMovieArgument {
  int? id;

  DetailMovieArgument({this.id});
}

class DetailMovieScreen extends StatefulWidget {
  DetailMovieScreen({Key? key}) : super(key: key);

  @override
  State<DetailMovieScreen> createState() => _DetailMovieScreenState();
}

class _DetailMovieScreenState extends State<DetailMovieScreen> {
  var controller = Get.find<DetailMovieController>();
  var creditsMovie = Get.find<MovieCreditsController>();

  @override
  void initState() {
    // TODO: implement initState
    DetailMovieArgument argument = Get.arguments;
    controller.id.value = argument.id;
    creditsMovie.id.value = argument.id;
    controller.getDetailMovie();
    creditsMovie.getMovieCredits();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Obx(() {
              if (controller.detailMovie != null) {
                return Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          child: MovieImageNetwork(
                            imageUrl: '$imageUrl${controller.detailMovie.value?.posterPath}',
                            fit: BoxFit.fill,
                            width: screenWidth(context),
                            height: screenHeight(context, dividedBy: 1.7),
                            borderRadius: BorderRadius.all(Radius.circular(0)),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            height: 350.0,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              gradient: LinearGradient(
                                begin: FractionalOffset.topCenter,
                                end: FractionalOffset.bottomCenter,
                                colors: [
                                  Colors.grey.withOpacity(0.0),
                                  Colors.black,
                                ],
                                stops: [0.0, 1.0],
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Row(
                                  children: [
                                    Flexible(
                                      child: AutoSizeText(
                                        '${controller.detailMovie.value?.title ?? ''}',
                                        maxLines: 2,
                                        softWrap: true,
                                        maxFontSize: 30,
                                        style: Theme.of(context).textTheme.headline4?.copyWith(
                                              color: AppTheme.whiteColor,
                                              fontSize: 60,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                                Gap(8),
                                if (controller.detailMovie.value?.voteAverage != null)
                                  Row(
                                    children: [
                                      RatingBarIndicator(
                                        rating: controller.detailMovie.value!.voteAverage! / 2.0,
                                        itemCount: 5,
                                        itemBuilder: (context, index) => Icon(
                                          Icons.star,
                                          color: Colors.yellow,
                                        ),
                                        itemSize: 24,
                                      ),
                                      Gap(8),
                                      Text('${controller.detailMovie.value!.voteAverage}'),
                                      Gap(8),
                                    ],
                                  ),
                                Gap(8),
                                if (controller.detailMovie.value?.genres != null)
                                  Wrap(
                                    children: List<Widget>.generate(
                                      controller.detailMovie.value!.genres!.length,
                                      (index) => Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 2),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: AppTheme.primaryColor,
                                              borderRadius: BorderRadius.circular(
                                                  20) // use instead of BorderRadius.all(Radius.circular(20))
                                              ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                                            child: Text(
                                              '${controller.detailMovie.value?.genres![index].name}',
                                              style: GoogleFonts.roboto(color: AppTheme.whiteColor, fontSize: 12),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding:  EdgeInsets.only(top: MediaQuery.of(context).padding.top,left:16),
                          child: CircleAvatar(
                            backgroundColor: AppTheme.textColor,
                            foregroundColor: Colors.white,
                            child: IconButton(
                              icon: Icon(Icons.arrow_back_ios),
                              onPressed: () {
                                Get.back();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                    if (controller.detailMovie.value?.runtime != null)
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Text('${formatDateYearOnly(controller.detailMovie.value!.releaseDate)} '), ///TODO year
                            ///Release
                            Text('• '),
                            Text(
                              _showDuration(controller.detailMovie.value!.runtime!),
                            ),
                          ],
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          AutoSizeText(
                            '${controller.detailMovie.value?.overview ?? ''}',
                            maxLines: 6,
                            softWrap: true,
                            minFontSize: 14,
                            maxFontSize: 18,
                            style: Theme.of(context).textTheme.bodyText2?.copyWith(color: AppTheme.whiteColor),
                          ),
                          Gap(8),
                        ],
                      ),
                    ),
                  ],
                );
              }
              if (controller.isLoading) {
                return ShimmerHeaderMovie();
              }
              return Container();
            }),
            Gap(8),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Cast',
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
            Obx(
              () {
                if (creditsMovie.creditsMovie != null) {
                  return Container(
                    height: 120,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: creditsMovie.creditsMovie.value?.cast?.length,
                      itemBuilder: (context, index) {
                        var data = creditsMovie.creditsMovie.value?.cast?[index];
                        return Container(
                          width: 100,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(8), // Border width
                                decoration: BoxDecoration(shape: BoxShape.circle),
                                child: ClipOval(
                                  child: SizedBox.fromSize(
                                    size: Size.fromRadius(40), // Image radius
                                    child: MovieImageNetwork(imageUrl: '$imageUrl${data?.profilePath}', fit: BoxFit.cover),
                                  ),
                                ),
                              ),
                              AutoSizeText(
                                '${data?.name ?? ''}',
                                maxLines: 1,
                                softWrap: true,
                                textAlign: TextAlign.center,
                                maxFontSize: 13,
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    ?.copyWith(color: AppTheme.whiteColor),
                              ),
                              // Flexible(child: Text('${data?.name}',maxLines: 2,))
                            ],
                          ),
                        );
                      },
                    ),
                  );
                }
                if (creditsMovie.isLoading) {
                  return Container();
                }
                return Container();
              },
            ),
            Gap(8),
          ],
        ),
      ),
    );
  }
}

String _showDuration(int runtime) {
  final int hours = runtime ~/ 60;
  final int minutes = runtime % 60;

  if (hours > 0) {
    return '${hours}h ${minutes}min';
  } else {
    return '${minutes}m';
  }
}
