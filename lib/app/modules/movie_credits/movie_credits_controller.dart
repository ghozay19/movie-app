import 'package:elemes_movie_app/app/data/network/model/detail/detail_movie_response.dart';
import 'package:elemes_movie_app/app/data/network/model/movie_credits/movie_credits_response.dart';
import 'package:elemes_movie_app/app/data/network/model/movie_response.dart';
import 'package:elemes_movie_app/app/data/network/provider/api_provider.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class MovieCreditsController extends GetxController {

  var logger = Logger();
  final _repository = ApiProvider();

  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;

  bool get isEmpty => _isEmpty.value;
  bool get isFailed => _isFailed.value;
  bool get isLoading => _isLoading.value;

  Rxn<int> id = Rxn<int>();
  Rxn<MovieCreditsResponse> creditsMovie = Rxn<MovieCreditsResponse>();

  Future getMovieCredits() async {
    _isLoading(true);
    final data = await _repository.doGetMovieCredits(id: id.value);
    if(data==null){
      _isFailed.value = true;
    } else {
      creditsMovie.value = data;
    }
    _isLoading(false);
  }

}