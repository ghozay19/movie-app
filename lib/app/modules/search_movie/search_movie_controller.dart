import 'package:elemes_movie_app/app/data/network/model/movie_response.dart';
import 'package:elemes_movie_app/app/data/network/model/results.dart';
import 'package:elemes_movie_app/app/data/network/provider/api_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';


class SearchMovieController extends GetxController {
  var logger = Logger();
  final _repository = ApiProvider();

  final _listRecentNews = <Movie>[].obs;
  List<Movie> get listFewRecentNews => _listRecentNews;
  Rxn<MovieResponse> _searchNewsResponse = Rxn<MovieResponse>();
  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;
  final _isFirstInit = true.obs;
  bool get isEmpty => _isEmpty.value;
  bool get isFailed => _isFailed.value;
  bool get isLoading => _isLoading.value;
  MovieResponse? get searchNewsResponse => _searchNewsResponse.value;

  final _page = 1.obs;
  int get page => _page.value;
  int get count => _listRecentNews.length;
  final _hasReachedMax = false.obs;
  bool get isFirstInit => _isFirstInit.value;
  bool get isFirstPage => page == 1;
  bool get isLoadingFirst => isLoading && isFirstPage;
  bool get isLoadingMore => isLoading && page > 1;
  bool get hasReachedMax => _hasReachedMax.value;
  List<Movie> get items => _listRecentNews.toList();
  ItemScrollController itemScrollController = ItemScrollController();
  ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();

  final queryTextEditingController = TextEditingController();
  var queryText = ''.obs;

  @override
  void onInit() {
    super.onInit();
    queryTextEditingController.addListener(() {
      queryText.value = queryTextEditingController.text;
      Get.put(Logger()).d('message ${queryText.value}');
    });
  }

  @override
  void onReady() {
    super.onReady();
    _trigger();
  }

  Future doSearchNews({required bool isRefresh, required bool isSearch, String? query}) async {
    if (isRefresh == true) {
      _searchNewsResponse.value == null;
      _isFirstInit.value = false;
      _listRecentNews.value = [];
      _page.value = 1;
      _isEmpty.value = false;
      _isFailed.value = false;
      _isLoading(true);
    }

    if (isSearch == true) {
      logger.d('${_listRecentNews.length}');
      _isLoading(false);
      _isFirstInit.value = false;
      _listRecentNews.value = [];
      _listRecentNews.clear();
      _page.value = 1;
      _isEmpty.value = false;
      _isFailed.value = false;
    }
    final data = await _repository.doSearchMovie(page: 1, query: queryText.value);

    if (data.movies == null) {
      _isFailed.value = true;
      _searchNewsResponse.value == null;
    } else if (data.movies!.isEmpty) {
      _isEmpty.value = true;
    } else {
      _searchNewsResponse.value = data;
      _listRecentNews.addAll(data.movies!);
      logger.d('message $count');
    }
    _page.value = 2;
    _isLoading(false);
  }

  Future<void> _loadPerPage() async {
    _isLoading(true);

    final items = await _repository.doSearchMovie(page: 1, query: queryText.value);

    if (items != null) {
      if (items.movies!.isEmpty) {
        _isFirstInit.value = false;
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = true;
      } else {
        _listRecentNews.addAll(items.movies!);
        _isFirstInit.value = false;
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = false;
        _page.value++;
      }
    }
    _isLoading(false);
  }

  void _trigger() {
    itemPositionsListener.itemPositions.addListener(() {
      final pos = itemPositionsListener.itemPositions.value;
      final lastIndex = count - 1;

      final isAtBottom = pos.isNotEmpty && pos.last.index == lastIndex;
      final isLoadMore = isAtBottom && !isLoading && !hasReachedMax;

      // load data from the next page
      if (isLoadMore) {
        _loadPerPage();
      }
    });
  }
}
