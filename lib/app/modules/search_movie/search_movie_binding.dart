import 'package:elemes_movie_app/app/modules/search_movie/search_movie_controller.dart';
import 'package:get/get.dart';

class SearchMovieBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchMovieController>(() => SearchMovieController());
  }
}
