import 'package:elemes_movie_app/app/data/network/model/detail/detail_movie_response.dart';
import 'package:elemes_movie_app/app/data/network/provider/api_provider.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class DetailMovieController extends GetxController {

  var logger = Logger();
  final _repository = ApiProvider();

  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;

  bool get isEmpty => _isEmpty.value;
  bool get isFailed => _isFailed.value;
  bool get isLoading => _isLoading.value;

  Rxn<int> id = Rxn<int>();
  Rxn<DetailMovieResponse> detailMovie = Rxn<DetailMovieResponse>();

  Future getDetailMovie({bool isInitialFetching = false}) async {
    if (isInitialFetching == true) _isLoading(true);
    final data = await _repository.doDetailMovie(id: id.value);
    if(data==null){
      _isFailed.value = true;
    } else {
      detailMovie.value = data;
    }
    _isLoading(false);
  }





}