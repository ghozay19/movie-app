import 'package:elemes_movie_app/app/modules/detail/detail_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/movie_credits/movie_credits_controller.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';


class DetailMovieBinding extends Bindings {
  var logger = Logger();

  @override
  void dependencies() {
Get.lazyPut(() => DetailMovieController());
    Get.lazyPut(() => MovieCreditsController());
  }
}
