import 'package:carousel_slider/carousel_slider.dart';
import 'package:elemes_movie_app/app/data/network/model/results.dart';
import 'package:elemes_movie_app/app/data/network/provider/api_provider.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class TrendingMovieController extends GetxController {
  var logger = Logger();
  final _repository = ApiProvider();
  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;

  final _isSliderLoading = false.obs;
  final _isSliderEmpty = false.obs;
  final _isSliderFailed = false.obs;

  bool get isSliderEmpty => _isSliderEmpty.value;
  bool get isSliderFailed => _isSliderFailed.value;
  bool get isSliderLoading => _isSliderFailed.value;

  bool get isEmpty => _isEmpty.value;
  bool get isFailed => _isFailed.value;
  bool get isLoading => _isLoading.value;

  final _listSliderPopular = <Movie>[].obs;
  final _listTrending = <Movie>[].obs;
  List<Movie> get listSliderNews => _listSliderPopular;
  List<Movie> get listTrending => _listTrending;
  CarouselController carouselController = CarouselController();
  final currentSlider = 0.obs;

  final _hasReachedMax = false.obs;
  final _page = 1.obs;
  int get page => _page.value;
  int get count => _listTrending.length;
  bool get isFirstPage => page == 1;
  bool get isLoadingFirst => isLoading && isFirstPage;
  bool get isLoadingMore => isLoading && page > 1;
  bool get hasReachedMax => _hasReachedMax.value;
  List<Movie> get items => _listTrending.toList();
  ItemScrollController itemScrollController = ItemScrollController();
  ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();

  Future getTrendingWeekSliderMovie({bool isInitialFetching = false, bool isRefresh = false}) async {
    if (isInitialFetching == true) {
      _isSliderLoading(true);
      _isSliderFailed.value = false;
      _listSliderPopular.clear();
    }
    if (isRefresh == true) {
      _isSliderFailed.value = false;
      _listSliderPopular.clear();
    }
    final data = await _repository.doGetTrendingMovie(type: 'week',page: 1);

    if (data.movies == null) {
      _isSliderFailed.value = true;
    } else if (data.movies!.isEmpty) {
      _isSliderEmpty.value = true;
    } else {
      _listSliderPopular.addAll(data.movies!);
    }
    _isSliderLoading(false);
  }



  Future getTrendingWeek({required bool isRefresh}) async {
    _isLoading(true);
    if (isRefresh) {
      _listTrending.clear();
      _page.value = 1;
      _isEmpty.value = false;
      _isFailed.value = false;
    }
    final data = await _repository.doGetNowPlayingMovie(page: 1);
    if (data.movies == null) {
      _isFailed.value = true;
    } else if (data.movies!.isEmpty) {
      _isEmpty.value = true;
    } else {
      _listTrending.addAll(data.movies!);
    }
    _page.value = 2;
    _isLoading(false);
  }

  Future<void> _loadPerPage() async {
    _isLoading(true);

    final items = await _repository.doGetNowPlayingMovie(page: 1);

    if (items != null) {
      if (items.movies!.isEmpty) {
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = true;
      } else {
        _listTrending.addAll(items.movies!);
        _isEmpty.value = false;
        _isFailed.value = false;
        _hasReachedMax.value = false;
        _page.value++;
      }
    }
    _isLoading(false);
  }

  void _trigger() {
    itemPositionsListener.itemPositions.addListener(() {
      final pos = itemPositionsListener.itemPositions.value;
      final lastIndex = count - 1;

      final isAtBottom = pos.isNotEmpty && pos.last.index == lastIndex;
      final isLoadMore = isAtBottom && !isLoading && !hasReachedMax;

      // load data from the next page
      if (isLoadMore) {
        _loadPerPage();
      }
    });
  }



  @override
  void onInit() async{
    // TODO: implement onReady
    super.onInit();
    getTrendingWeekSliderMovie(isRefresh: true, isInitialFetching: true);
  }


  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
    getTrendingWeek(isRefresh: true);
    _trigger();
  }
}