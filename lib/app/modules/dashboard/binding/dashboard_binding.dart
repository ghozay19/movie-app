import 'package:elemes_movie_app/app/modules/dashboard/controller/dashboard_controller.dart';
import 'package:elemes_movie_app/app/modules/now_playing_movie/nowplaying_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/popular_movie/popular_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/top_rated_movie/top_rated_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/trending_movie/trending_movie_controller.dart';
import 'package:elemes_movie_app/app/modules/upcoming_movie/upcoming_movie_controller.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';


class DashboardBinding extends Bindings {
  var logger = Logger();

  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<PopularMovieController>(() => PopularMovieController());
    Get.lazyPut<UpComingMovieController>(() => UpComingMovieController());
    Get.lazyPut<NowPlayingMovieController>(() => NowPlayingMovieController());
    Get.lazyPut<TopRatedMovieController>(() => TopRatedMovieController());
    Get.lazyPut<TrendingMovieController>(() => TrendingMovieController());
  }
}
