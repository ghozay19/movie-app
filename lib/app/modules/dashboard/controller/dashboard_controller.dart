import 'package:carousel_slider/carousel_slider.dart';
import 'package:elemes_movie_app/app/modules/popular_movie/popular_movie_controller.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class DashboardController extends GetxController {
  @override
  void onInit() async {
  // await  callFirstAPI();
    super.onInit();
  }

  var logger = Logger();

  Future<bool> callFirstAPI() async {
    logger.d('message');
    Future.microtask(() {
      Get.find<PopularMovieController>().getPopularSliderMovie(isRefresh: true, isInitialFetching: true);
    });
    return false;
  }
}
