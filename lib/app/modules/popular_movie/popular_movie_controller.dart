import 'package:carousel_slider/carousel_slider.dart';
import 'package:elemes_movie_app/app/data/network/model/results.dart';
import 'package:elemes_movie_app/app/data/network/provider/api_provider.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';

class PopularMovieController extends GetxController {
  var logger = Logger();
  final _repository = ApiProvider();
  final _isLoading = true.obs;
  final _isEmpty = false.obs;
  final _isFailed = false.obs;

  final _isSliderLoading = false.obs;
  final _isSliderEmpty = false.obs;
  final _isSliderFailed = false.obs;

  bool get isSliderEmpty => _isSliderEmpty.value;
  bool get isSliderFailed => _isSliderFailed.value;
  bool get isSliderLoading => _isSliderFailed.value;

  final _listSliderPopular = <Movie>[].obs;
  List<Movie> get listSliderNews => _listSliderPopular;
  CarouselController carouselController = CarouselController();
  final currentSlider = 0.obs;


  Future getPopularSliderMovie({bool isInitialFetching = false, bool isRefresh = false}) async {
    if (isInitialFetching == true) {
      _isSliderLoading(true);
      _isSliderFailed.value = false;
      _listSliderPopular.clear();
    }
    if (isRefresh == true) {
      _isSliderFailed.value = false;
      _listSliderPopular.clear();
    }
    final data = await _repository.doGetPopularMovie(page: 1);

    if (data.movies == null) {
      _isSliderFailed.value = true;
    } else if (data.movies!.isEmpty) {
      _isSliderEmpty.value = true;
    } else {
      _listSliderPopular.addAll(data.movies!);
    }
    _isSliderLoading(false);
  }


  @override
  void onInit() async{
    // TODO: implement onReady
    super.onInit();
    getPopularSliderMovie(isRefresh: true, isInitialFetching: true);
  }

}