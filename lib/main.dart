import 'dart:async';

import 'package:elemes_movie_app/app/app.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';



void main() async {
  await mainProgram();
}


Future<void> mainProgram() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  initializeDateFormatting();
  runApp(ElemesMovieApp());
}